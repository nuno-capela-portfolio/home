package org.academiadecodigo.red.Mechanics;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class UserPaddle implements KeyboardHandler {
    private Picture pad;
    private MapPosition paddlePos;
    private GameField field;
    private int speedPad;
    private String filePath;
    private boolean pause;

    public UserPaddle(GameField field, int chosenLevel) {
        if (chosenLevel == 1) {
            filePath = "resources/Goalkeeper.png";
        }
        if (chosenLevel == 2) {
            filePath = "resources/volleyPlayer.png";
        }
        if (chosenLevel == 3) {
            filePath = "resources/astronauta.png";
        }
        this.field = field;
        paddlePos = new MapPosition(10, field.getFieldPos().lowerPos() - 50,
                100, 5);

        // Construction needs thickness, not position
        pad = new Picture(paddlePos.drawPos()[0], paddlePos.drawPos()[1],
                filePath);

        // Speed to user move the pad
        speedPad = 25;
    }

    public void init() {
        keyboardInit();
        pad.draw();
    }

    private void keyboardInit() {
        Keyboard keyboard = new Keyboard(this);

        //RIGHT KEY to move right
        KeyboardEvent rightKeyPressed = new KeyboardEvent();
        rightKeyPressed.setKey(KeyboardEvent.KEY_RIGHT);
        rightKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightKeyPressed);

        //LEFT KEY to move left
        KeyboardEvent leftKeyPressed = new KeyboardEvent();
        leftKeyPressed.setKey(KeyboardEvent.KEY_LEFT);
        leftKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftKeyPressed);
    }

    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (pause) {
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT
                    && paddlePos.leftPos() > field.getFieldPos().leftPos()) {

                // Update User Pad position base on movement
                paddlePos.setXY(paddlePos.leftPos() - speedPad, paddlePos.upperPos());
                pad.translate(-speedPad, 0);
            }

            if (keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT
                    && paddlePos.rightPos() < field.getFieldPos().rightPos()) {

                // Update User Pad position base on movement
                paddlePos.setXY(paddlePos.leftPos() + speedPad, paddlePos.upperPos());
                pad.translate(speedPad, 0);
            }
        }
        //System.out.println("Pad pos" + Arrays.toString(startXY));
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public MapPosition getPaddlePos() {
        return paddlePos;
    }

    public void deletePad() {
        pad.delete();
    }


    public void setPause(boolean pause) {
        this.pause = pause;
    }
}
