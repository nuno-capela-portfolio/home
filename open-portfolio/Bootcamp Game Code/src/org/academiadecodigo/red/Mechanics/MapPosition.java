package org.academiadecodigo.red.Mechanics;

import java.util.HashMap;

public class MapPosition {
    private HashMap<String,Double> positionMapped= new HashMap();

    public MapPosition(double x, double y, double width, double height){
        positionMapped.put("StartX",x);
        positionMapped.put("StartY",y);
        positionMapped.put("Width",width);
        positionMapped.put("Height",height);
    }

    public double[] drawPos(){
        return new double[]{positionMapped.get("StartX"),
                            positionMapped.get("StartY"),
                                positionMapped.get("Width"),
                                    positionMapped.get("Height")};
    }
    public double leftPos(){
        return positionMapped.get("StartX");
    }
    public double rightPos(){
        return positionMapped.get("StartX") + positionMapped.get("Width");
    }
    public double upperPos(){
        return positionMapped.get("StartY");
    }
    public double lowerPos(){
        return positionMapped.get("StartY") + positionMapped.get("Height");
    }
    public double centerX(){
        return positionMapped.get("StartX") + positionMapped.get("Width")/2;
    }
    public double width(){
        return positionMapped.get("Width");
    }
    public double centerY(){
        return positionMapped.get("StartY") + positionMapped.get("Height")/2;
    }
    public double[] XY(){
        return new double[]{leftPos(),upperPos()};
    }

    public void setXY(double x, double y){
        positionMapped.put( "StartX", x);
        positionMapped.put( "StartY", y);
    }

    @Override
    public String toString(){
        return "Left: "+ leftPos() +
                ", Right: " + rightPos()+
                ", Upper: " + upperPos()+
                ", Lower: " + lowerPos();
    }
}
