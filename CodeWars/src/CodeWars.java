import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.lang.String;

public class CodeWars {

    public static void main(String[] args) {
        System.out.println( sqInRect(20,7));
    }

    //6 kyu Sum of Digits / Digital Root. Recursive sum of all the digits in a number
    public static int digital_root(int n) {
        if(n<10){return n;}
        String string = Integer.toString(n);
        char[] array = string.toCharArray();
        int result=0;
        for (char c : array) {
            result +=  Character.getNumericValue(c);
        }
        return digital_root (result);
    }

    //6 kyu Rectangle into Squares -- cut a given "true" rectangle into squares ("true" rectangle meaning that the two dimensions are different).
    public static List<Integer> sqInRect(int lng, int wdth) {
        if(lng==wdth){return null;}
        LinkedList<Integer> result = new LinkedList();                    
        if (wdth==0||lng==0){return result;}
        if(wdth>lng){
            Integer temp = lng;
            lng=wdth;
            wdth=temp;
        }
        if(wdth==1){
            for(int i = 1; i<=lng;i++) {
                result.add(1);
            }
            return result;
        }
        result.add(wdth);
        if(lng-wdth==wdth){
            result.add(wdth);
            return result;
        }
        result.addAll(sqInRect(lng-wdth,wdth));
        return result;
    }

    //Dot Calculator -- You have to write a calculator that receives strings for input. The dots will represent the number in the equation.
    public static String calc(String txt){
            String str=txt.replaceAll(" ","");
            str = str.replaceAll("//","/");
            System.out.println(str.toString());
            String [] operators = str.split("[*-+/]");
            System.out.println(operators[0].toString());
            System.out.println(operators[1].toString());
            int a = operators[0].length();
            int b = operators[1].length();
            System.out.println(a+"    "+b);
            System.out.println(str.toString());
            str=str.replaceAll(".", "");
            System.out.println("str "+str.toString());
            System.out.println("a: "+a + " b: "+b);
            switch (str) {
                case "+":return ".".repeat(a+b);
                case "-":return ".".repeat(a-b);
                case "*":return ".".repeat(a*b);
                case "/":return ".".repeat(a/b);
                str.
            }
            return "";
    }

    //7 kyu Help Suzuki complete his chores! Suzuki has a long list of chores required to keep the monastery in good order. Each chore can be completed independent of the others and assigned to any student. He needs to assign two chores to each student in a way which minimizes the total duration of the day's work. There will always be an even number of chores and enough students to complete them.You will be given an array containing the estimated duration of each chore
    public static int[] choreAssignments(int[] chores){
        Arrays.sort(chores);
        int half=chores.length/2;
        int[] result = new int[half];
        for (int i=0; i<half;i++){
            result[i]= chores[i]+chores[chores.length-i-1];
        }
        Arrays.sort(result);
        return result;
    }

    //7 kyu Scrabble Score
    public static int scrabbleScore(String word) {
        char[] letters = word.toLowerCase().toCharArray();
        int result = 0;
        for (int i=0; i < letters.length;i++){
            switch (letters[i]) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'l':
                case 'n':
                case 'r':
                case 's':
                case 't':
                    result += 1;
                    break;
                case 'd':
                case 'g':
                    result += 2;
                    break;
                case 'b':
                case 'c':
                case 'm':
                case 'p':
                    result += 3;
                    break;
                case 'f':
                case 'h':
                case 'v':
                case 'w':
                case 'y':
                    result += 4;
                    break;
                case 'k':
                    result += 5;
                    break;
                case 'j':
                case 'x':
                    result += 8;
                    break;
                case 'q':
                case 'z':
                    result += 10;
                    break;
            }
        }
        return result;
    }

    //7 kyu Sum of odd numbers
    public static int rowSumOddNumbers(int n) {
        return n * n * n;
    }

    //7 kyu Two to One (Take 2 strings s1 and s2 including only letters from a to z. Return a new sorted string, the longest possible, containing distinct letters - each taken only once - coming from s1 or s2.)
    public static String longest (String s1, String s2) {
        char[] chars1 = s1.toCharArray();
        char[] chars2 = s2.toCharArray();
        String result = new String();
        for (int i=0; i<chars1.length; i++){
            if(result.indexOf(chars1[i])==-1) {
                result+=(chars1[i]);
            }
        }
        for (int j=0; j<chars2.length; j++){
            if(result.indexOf(chars2[j])==-1) {
                result+=(chars2[j]);
            }
        }
        char[] tempResult = result.toCharArray();
        Arrays.sort(tempResult);
        return new String (tempResult);
    }

    //7 kyu Looking for a benefactor(The function new_avg(arr, navg) should return the expected donation (rounded up to the next integer) that will permit to reach the average)
    public static long newAvg(double[] arr, double navg) throws IllegalArgumentException{
        double totalN=0;
        for (double i : arr) {totalN += i;}
        Long result = (long) Math.ceil(((arr.length+1)*navg)-totalN);
        if (result<=0) {throw new IllegalArgumentException("deu coco");}
        else {return result;}
    }

    //7 kyu The highest profit wins! Write a function that returns both the minimum and maximum number of the given list/array.
    public static int[] minMax(int[] arr) {
        int[] result = {arr[0],arr[0]};
        for (int x : arr) {
            if (x > result[1]) {
                result[1] = x;
            }
            if (x < result[0]) {
                result[0] = x;
            }
        }
        return result;
    }

    //7 kyu Disemvowel Trolls
    public static String disemvowel(String str) {
        return str.replaceAll("[aeiouAEIOU]", "");
    }

    //7 kyu The fusc function -- Part 1  -- 1. fusc(0) = 0 -- 2. fusc(1) = 1 -- 3. fusc(2 * n) = fusc(n) -- 4. fusc(2 * n + 1) = fusc(n) + fusc(n + 1)
    public static int fusc(int n) {
            if (n==0||n==1) {return n;}
            if (n%2==0){return fusc(n/2);}
            return fusc(n/2) + fusc(n/2+1);
        }

    //8 kyu Counting sheep... (count thre true's in an array of booleans
    public int countSheeps(Boolean[] arrayOfSheeps) {
        return (int)Arrays.stream(arrayOfSheeps).filter(sheep -> sheep!=null).filter(sheep -> sheep/*==true*/).count();
    }

    //8 kyu Regexp Basics - is it a digit? is it a digit?
    public static boolean isDigit(String s) {
        return s.length()!=1? false :"1234567890".contains(s);
    }

    //8 kyu Remove String Spaces
    public static String noSpace(final String x) {
        return x.replaceAll(" ", "");
    }

    //8 kyu Correct the mistakes of the character recognition software
    public static String correct(String string) {
        return string=string.replace('5','S').replace('0','O').replace('1','I');
    }

    //8 kyu A wolf in sheep's clothing
    public static String warnTheSheep(String[] array) {
        if (array[array.length-1].equals("wolf")) {return "Pls go away and stop eating my sheep";}
        int lineCount=1;
        for (int i=array.length-2 ; i >=0; i--){
            if (array[i]=="wolf"){break;}
            lineCount++;
        }
        return "Oi! Sheep number " + lineCount + "! You are about to be eaten by a wolf!";
    }

    //8 kyu Opposite number
    public static int opposite(int number) {
        return -number;
    }

    //8 kyu Exclusive "or" (xor) Logical Operator
    public static boolean xor(boolean a, boolean b) {
        if (a){return !b;}
        return b;
    }

    //8 kyu String repeat (Write a function that accepts an integer n and a string s as parameters, and returns a string of s repeated exactly n times.)
    public static String repeatStr(final int repeat, final String string) {
        int i=repeat;
        String s = new String("");
        while (i>0) {
            s=s+string;
            i--;
        }
        return s;
    }

    //8 kyu Will you make it? (write a function that tells you if it is possible to get to the pump or not.)
    public static boolean zeroFuel(double distanceToPump, double mpg, double fuelLeft) {
        return distanceToPump <= mpg * fuelLeft ? true : false;
    }

    //8 kyu Expressions Matter (Given three integers a ,b ,c, return the largest number obtained after inserting the following operators and brackets: +, *, () . )
    public static int expressionsMatter(int a, int b, int c) {
        int middle = highestResult (a,b);
        int end = highestResult (middle,c);
        int middle2 = highestResult (b,c);
        int end2 = highestResult (middle2,a);
        return end > end2? end: end2;
    }
    /* helps with expressions matter*/public static int highestResult (int x, int y) {
        int a=(x*y);
        int b=(x+y);
        return a > b? a : b;
    }

    //8 kyu How many stairs will Suzuki climb in 20 years? (Suzuki is a monk who climbs a large staircase to the monastery as part of a ritual. Some days he climbs more stairs than others depending on the number of students he must train in the morning. He is curious how many stairs might be climbed over the next 20 years and has spent a year marking down his daily progress.)
    public static long stairsIn20(int[][] stairs) {
        int sum = 0;
        for (int i = 0; i < stairs.length; i++) {
            for (int j = 0; j < stairs[i].length; j++) {
                sum += stairs[i][j];
            }
        }
        return sum * 20;
    }

    //8 kyu Replace all dots (replace all the dots . in the specified String str with dashes -)
    public static String replaceDots(final String str) {
        return str.replaceAll("\\.", "-");
    }

    //8 kyu Volume of a Cuboid - calculate the volume of a cuboid with three values: the length, width and height
    public static double getVolumeOfCuboid(final double length, final double width, final double height) {
        return length*width*height;
    }

    //8 kyu L1: Set Alarm
    public static boolean setAlarm(boolean employed, boolean vacation) {
        return employed && !vacation;
    }

    //8 kyu Convert boolean values to strings 'Yes' or 'No'
    class YesOrNo {
        public String boolToWord (boolean b){ return b ? "Yes" : "No"; }
    }




/*
    "Which users have borrowed books published before 1974?"


    select U.name
            from users as U
            where U.user_id in (

    select distinct L.user_id
        from loans as L

    where L.book_id IN (
            select B.book_id
            from books as B
            where B.published_date < "1974-01-01"));
*/
}
